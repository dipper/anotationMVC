package demo.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import demo.form.DemoForm;
import struts.core.action.Action;
import struts.core.annotation.ActionAno;
import struts.core.annotation.AnnotationRedirct;
import struts.core.form.ActionForm;

/** 
* @ClassName: DemoAction 
* @Description: 注解列子 
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午5:43:06 
*  
*/
public class DemoAction implements Action{

	public DemoAction() {
		// TODO Auto-generated constructor stub
	}

	@ActionAno(path="/demo",form="demo.form.DemoForm")
	@AnnotationRedirct(resultType={"success","fail"},redirctUrl={"view/success.jsp","view/fail.jsp"})
	@Override
	public String excute(HttpServletRequest request,
			HttpServletResponse response, ActionForm form) {
		
		DemoForm demoform = (DemoForm) form;
		String url = "fail";
		if("zx".equals(demoform.getUsername())){
			url = "success";
		}
		
		return url;
	}

	
}
