package struts.core.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import struts.core.annotation.ActionAno;
import struts.core.annotation.AnnotationRedirct;

/** 
* @ClassName: AnnotationUtil 
* @Description: 扫描并分析注解 
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午5:14:45 
*  
*/
public class AnnotationUtil {

	public static Map<String,AnnotationBean> getAnnotaionConfig(String packageName){
		
		Map<String,AnnotationBean> map = new HashMap<String,AnnotationBean>();
		
		List<String> scanclassList =  ScanClassesInPackage.scanClasses(packageName);
		
		for(String className:scanclassList){
			try {
				Class<?> clazz = Class.forName(className);
				
				Method[] methods = clazz.getDeclaredMethods();

				for(Method method:methods){
					AnnotationBean bean = new AnnotationBean();
					if(method.isAnnotationPresent(ActionAno.class)){
						ActionAno actionano = method.getAnnotation(ActionAno.class);
						String path = actionano.path();
						String formclass = actionano.form();
						bean.setActionPath(path);
						bean.setFormbeanClass(formclass);
						bean.setActionClass(className);
						bean.setActionMethod(method.getName());
						if(method.isAnnotationPresent(AnnotationRedirct.class)){
							
							AnnotationRedirct redirct = method.getAnnotation(AnnotationRedirct.class);
							String types[] = redirct.resultType();
							String url[] = redirct.redirctUrl();
							for(int i=0;i<types.length;i++){
								bean.getForwards().put(types[i], url[i]);
							}
						}
					}
					map.put(bean.getActionPath(), bean);
				}
				
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.out.println("类名找不到");
			}
		}
		return map;
	}
}
