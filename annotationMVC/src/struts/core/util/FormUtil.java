package struts.core.util;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

import struts.core.form.ActionForm;

/** 
* @ClassName: FormUtil 
* @Description: 通过反射机制设置form里的数据
* @author zx zx2009428@163.com
* @date 2014年12月18日 上午10:54:55 
*  
*/
public class FormUtil {

	public FormUtil() {
	}
	
	public static ActionForm getForm(String formClass,HttpServletRequest resq){
		ActionForm  form = null;
		try{
			Class clazz = Class.forName(formClass);
			form = (ActionForm) clazz.newInstance();
			Field[] fieldarry = clazz.getDeclaredFields();
			
			for(Field field: fieldarry){
				field.setAccessible(true);
				field.set(form, resq.getParameter(field.getName()));
				field.setAccessible(false);
			}
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("Form加载错误。");
		}
		
		
		return form;
	}

}
