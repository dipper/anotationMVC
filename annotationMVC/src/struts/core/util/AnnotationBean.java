package struts.core.util;

import java.util.HashMap;
import java.util.Map;

/** 
* @ClassName: AnnotationBean 
* @Description: xml配置文件对应的javabean
* @author zx zx2009428@163.com
* @date 2014年12月18日 上午10:33:33 
*  
*/
public class AnnotationBean {

	public AnnotationBean() {
	}
	
	private String actionName="";
	private String actionClass="";
	private String actionPath="";
	private String formbeanName="";
	private String formbeanClass="";
	private String actionMethod = "";
	private Map<String,String> forwards= new HashMap<String,String>();
	public String getActionName() {
		return actionName;
	}
	public void setActionName(String actionName) {
		this.actionName = actionName;
	}
	public String getActionClass() {
		return actionClass;
	}
	public void setActionClass(String actionClass) {
		this.actionClass = actionClass;
	}
	public String getActionPath() {
		return actionPath;
	}
	public void setActionPath(String actionPath) {
		this.actionPath = actionPath;
	}
	public String getFormbeanName() {
		return formbeanName;
	}
	public void setFormbeanName(String formbeanName) {
		this.formbeanName = formbeanName;
	}
	public String getFormbeanClass() {
		return formbeanClass;
	}
	public void setFormbeanClass(String formbeanClass) {
		this.formbeanClass = formbeanClass;
	}
	public Map<String, String> getForwards() {
		return forwards;
	}
	public void setForwards(Map<String, String> forwards) {
		this.forwards = forwards;
	}
	public String getActionMethod() {
		return actionMethod;
	}
	public void setActionMethod(String actionMethod) {
		this.actionMethod = actionMethod;
	}
	
	

}
