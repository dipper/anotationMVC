package struts.core.util;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/** 
* @ClassName: ScanClassesInPackage 
* @Description: 扫描包里的类名
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午4:02:03 
*  
*/
public class ScanClassesInPackage {

	
	/** 
	* @Title: ScanClasses 
	* @Description: 扫描包里的所有类
	* @param @param packagename
	* 						要扫描的包名
	* @return List<String>    包里的所有类集合
	* @throws 
	*/
	public static List<String> scanClasses(String packagename){
		List<String> classesResult = new ArrayList<String>();
		//获得上下文的类加载器
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		
		String path = packagename.replace(".", "/");
		//查找具有给定名称的资源,资源名称是以 '/' 分隔的标识资源的路径名称。
		URL url = classLoader.getResource(path); 
		
		//获得要扫描的路径
		String filepath = url.getPath();
		
		//获得要扫描的文件
		File file = new File(filepath);
		
		//获得子文件
		File[] files = file.listFiles();
	
		for(File childfile:files){
			//如果是文件夹递归调用
			if(childfile.isDirectory()){
				String childpackagepath = packagename+"."+childfile.getName();
				//
				classesResult.addAll(scanClasses(childpackagepath));
			}else{
				//获得类的全路径名
				classesResult.add(packagename+"."+childfile.getName().replace(".class", ""));
			}
		}
		
		return classesResult;
	}
	
	public static void main(String[] args) {
		List<String> actionclass = ScanClassesInPackage.scanClasses("demo.action");
		for(String classname:actionclass){
			System.out.println(classname);
		}
	}
}
