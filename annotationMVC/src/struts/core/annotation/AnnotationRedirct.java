package struts.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
/** 
* @ClassName: AnnotationRedirct 
* @Description: 返回类型和路径注解
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午4:59:18 
*  
*/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AnnotationRedirct {
	public String[] resultType();
	public String[] redirctUrl();
}
