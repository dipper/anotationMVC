package struts.core.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
/** 
* @ClassName: ActionAno 
* @Description: 路径和form类注解
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午4:58:44 
*  
*/
import java.lang.annotation.Target;
/** 
* @ClassName: ActionAno 
* @Description: 方法请求注解
* @author zx zx2009428@163.com
* @date 2014年12月25日 下午5:44:03 
*  
*/
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ActionAno {

	String path();
	String form();
}
