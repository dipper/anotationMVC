package struts.core.action;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.core.form.ActionForm;

public interface Action {

	
	public String excute(HttpServletRequest request,HttpServletResponse response,ActionForm form);

}
