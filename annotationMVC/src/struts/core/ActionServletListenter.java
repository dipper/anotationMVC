package struts.core;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.http.HttpServlet;

import struts.core.util.AnnotationUtil;
import struts.core.util.StrutsXmlUtil;

public class ActionServletListenter implements ServletContextListener {

	public ActionServletListenter() {
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("系统已经销毁！");
	}

	@Override
	public void contextInitialized(ServletContextEvent servletcon) {
		ServletContext context = servletcon.getServletContext();
		String packageName = context.getInitParameter("scanpath");
		try{
		context.setAttribute("struts", AnnotationUtil.getAnnotaionConfig(packageName));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

 
}
