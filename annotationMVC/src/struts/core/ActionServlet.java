package struts.core;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.core.action.Action;
import struts.core.form.ActionForm;
import struts.core.util.FormUtil;
import struts.core.util.AnnotationBean;

public class ActionServlet extends HttpServlet {

	public ActionServlet() {
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		System.out.println(req.getServletPath());
		String actionPath = getPath(req.getServletPath());
		
		
		Map<String,AnnotationBean> annotationMap = (Map<String, AnnotationBean>) this.getServletContext().getAttribute("struts");
		
		AnnotationBean annotationbean = annotationMap.get(actionPath);
		ActionForm  form = FormUtil.getForm(annotationbean.getFormbeanClass(), req);
		
		String actionClass = annotationbean.getActionClass();
		Action action= null;
		String url = "";
		try{
			Class<?> clazz = Class.forName(actionClass);
			action = (Action) clazz.newInstance();
			Method method = clazz.getMethod(annotationbean.getActionMethod(), new Class[]{HttpServletRequest.class,HttpServletResponse.class,ActionForm.class});
			String type = (String) method.invoke(action, new Object[]{req, resp, form});
			url = annotationbean.getForwards().get(type);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("严重：action 错误");
		}
		
		RequestDispatcher dispatcher = req.getRequestDispatcher(url);
		dispatcher.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		doGet(req, resp);
	}
	
	private String getPath(String actionpath){
		
		return actionpath.split("\\.")[0];
	}
	

}
